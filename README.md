# Python知识图谱+B站资源整合 V1.0版本

| 项目持续维护中.......

项目包含：脑图、png图片和md文档。方便你在不同场景下使用学习。
不仅是一个自学的Python路径，项目包括Python自学的路径+知识图谱+B站免费视频的整合。

| 视频说明：[https://www.bilibili.com/video/BV1Ry4y1m7Ai](https://www.bilibili.com/video/BV1Ry4y1m7Ai)

你按照这个图谱学习Python，完全可以自学成功。

我还建立了一个微信自学群，方便和我一起进步学习。我的微信号：CoderWanFeng，加上我好友后，可以发送“我要自学”，我24小时内就会邀请你入群。
承诺：群内无收费、无广告、无买卖行为，只为学习使用。

由于微信号好友达到上限，没办法加人入群，推荐关注公众号“程序员晚枫”，每天有编程相关文章推送。

![程序员晚枫公众号](https://img-blog.csdnimg.cn/20201231105911656.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center)




![Python知识图谱+B站资源整合](https://img-blog.csdnimg.cn/20201231105911656.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMTUxNw==,size_16,color_FFFFFF,t_70#pic_center)


## 程序员晚枫的Python学习路径
B站学习资源整合
## 网络知识
（Internet）
- 1. 网络工作原理
- 计算机网络原理：https://www.bilibili.com/video/BV1xJ41137Q3
- 2. 什么是HTTP
- HTTP协议详解：https://www.bilibili.com/video/BV1js411g7Fw
- 3. 浏览器及工作方式
- 浏览器是如何运作的？：https://www.bilibili.com/video/BV1x54y1B7RE
- 4. DNS 及其工作原理
- DNS基本工作原理：https://www.bilibili.com/video/BV1GW411j7Ts
- 直观DNS科普：https://www.bilibili.com/video/BV1F54y1R7BC
- 5. 域名相关知识
- 域名解析完整讲解：https://www.bilibili.com/video/BV1zA411x7Pj
- 6. 云服务相关知识
- 揭秘阿里云服务器：https://www.bilibili.com/video/BV1Rt411u7k4

## 桌面应用开发
Desktop Applications
- Electron
- Electron入门：https://www.bilibili.com/video/BV1QB4y1F722
- Electron基础：https://www.bilibili.com/video/BV177411s7Lt





