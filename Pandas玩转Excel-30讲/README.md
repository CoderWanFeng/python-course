# 本代码仓库的下载指南


- 如果是代码小白，看一下这个5分钟的下载教程

[https://www.bilibili.com/video/BV1Ry4y1m7Ai](https://www.bilibili.com/video/BV1Ry4y1m7Ai)

- 如果是会使用Git的朋友，直接执行下列命令，下载所有源码到自己电脑
```
git clone https://gitee.com/CoderWanFeng/python-course.git
```
