# -*- coding: utf-8 -*-
# @公众号 :程序员晚枫 读者交流群：https://mp.weixin.qq.com/s/CadAaJUTUlXmTxJAjFUfPQ
# @Software: PyCharm 安装教程：https://mp.weixin.qq.com/s/a0zoCo9DacvdpIoz1LEN3Q
# @Description:
# Python全套学习资源：https://mp.weixin.qq.com/s/G_5cY05Qoc_yCXGQs4vIeg


"""
官方推荐：6种Pandas读取Excel的方法
原文链接：https://mp.weixin.qq.com/s/YkON0ROI7WPt0EefGYSEpg
"""
import pandas as pd

#
# 下面我们就根据上文获取到的pandas源码，逐个解析一下这6种读取excel的方式。
#
# 1、指定索引列读取
# 这种读取方式，适合Excel里的数据，本身有一列表示序号的情况。
#
pd.read_excel('fake2excel.xlsx', index_col=0)
#
# # 使用index_col=0，指定第1列作为索引列。
# 结果如下图所示：
#
# 列名没有对齐，不是代码运行有问题，是因为name列被当作了索引列（序号）。
# 图片
# 这种方式不符合我们这个文件的要求，所以我们可以进行以下修改：不要指定索引列。
#
# 代码和结果如下：
#
pd.read_excel('fake2excel.xlsx', index_col=None)
#
# 图片
#
# 2、指定sheet读取
# 见名知意。
#
pd.read_excel(open('fake2excel.xlsx', 'rb'), sheet_name='Sheet2')
#
# # 使用sheet_name=0，指定读取sheet2里面的内容。
# 我们在原表里加入了sheet2，结果如下图所示：
#
# 这种情况下，不会读取sheet1里面的内容
# 图片
#
#
# 3、取消header读取
# 读取本身没有列名的数据。
#
pd.read_excel('fake2excel.xlsx', index_col=None, header=None)
#
# # 使用header=None，取消header读取。
# 结果如下图所示：
#
# 这种情况下，适合原Excel表没有列名的情况。
# 我们的文件里有列名的情况下，列名也被当成了数据。
# 图片
#
#
# 4、指定读取格式
# 这种适合高端玩家，在对数据处理精度要求比较高或者速度要求比较快的情况下。
#
pd.read_excel('fake2excel.xlsx', index_col=0, dtype={'age': float})
#
# # 使用dtype，指定某一列的数据类型。
# 结果如下图所示：
#
# 我们添加了一列：年龄，本来是整数，但是指定float类型之后，读取出来成了小书。
# 这种读取，更适合对数据有特殊要求的情况。例如之前给大家分享过的：580页PDF：《Python金融大数据分析》
# 图片
#
#
# 5、自定义缺失值
# 这种使用的场景是什么呢？比如在收集信息的时候据时候，发现有人填的年龄是负数，那就自动给他把年龄清空掉，让他重新填写。
#
pd.read_excel('fake2excel.xlsx', index_col=None, na_values={'name': "庞强"})
#
# # 使用na_values，自己定义不显示的数据
# 结果如下图所示：
#
# 我们的表格里，有个人的名字叫：庞强我们不想显示这个人的名字
# 于是我们就在na_values指定：name这一列是庞强的名字，置为空，在pandas里空值会用NaN表示。
# 图片
#
#
# 6、处理Excel里的注释行
# 不仅Python是可以写注释的，Excel也是可以写注释的。很多人没有用过，用过的朋友在评论区说一下你为什么给Excel写注释吧~？
#
# pandas提供了处理Excel注释行的方法。
#
pd.read_excel('fake2excel.xlsx', index_col=None, comment='#')
# 结果如下图所示：
